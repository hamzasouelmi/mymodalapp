import React from 'react';
import {
  HStack,
  VStack,
  Text,
  IconButton,
  StackDivider,
  Spacer,
  Badge,
} from '@chakra-ui/react';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  FormLabel,
  Button,
  FormControl,
} from '@chakra-ui/react';
import { useState } from 'react';

import { FaTrash, FaEdit } from 'react-icons/fa';
import { Formiz, useForm } from '@formiz/core';
import { MyField } from './MyField';

function TodoList({ todos, deleteTodo, editTodo }) {
  const myForm = useForm();

  const [selectedId, setSelectedId] = useState();

  const initialRef = React.useRef(null);
  const finalRef = React.useRef(null);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const handleEditTodo = (value) => {
    if (!selectedId) return;

    editTodo({
      id: selectedId,
      body: value,
    });
    onClose();
  };

  const handleCloseModal = () => {
    setSelectedId(null);
    onClose();
  };

  const handleEditClick = (id) => {
    setSelectedId(id);
    onOpen();
  };

  if (!todos.length) {
    return (
      <Badge colorScheme="green" p="4" m="4" borderRadius="lg">
        No Todos, yay!!!
      </Badge>
    );
  }

  return (
    <>
      <VStack
        divider={<StackDivider />}
        borderColor="gray.100"
        borderWidth="2px"
        p="4"
        borderRadius="lg"
        w="100%"
        maxW={{ base: '90vw', sm: '80vw', lg: '50vw', xl: '40vw' }}
        alignItems="stretch"
      >
        {todos.map((todo) => (
          <HStack key={todo.id}>
            <Text>{todo.body}</Text>
            <Spacer />
            <IconButton
              icon={<FaTrash />}
              isRound="true"
              onClick={() => deleteTodo(todo.id)}
            />
            <IconButton
              icon={<FaEdit />}
              isRound="true"
              onClick={() => handleEditClick(todo.id)}
            />
          </HStack>
        ))}
      </VStack>

      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={handleCloseModal}
      >
        <Formiz
          onValidSubmit={(values) => {
            handleEditTodo(values.editName);
          }}
          connect={myForm}
          autoForm
        >
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>do you want to edit your todo</ModalHeader>
            <ModalCloseButton />
            <ModalBody pb={6}>
              <FormControl>
                <FormLabel>Your todo </FormLabel>
                <MyField name="editName" required="This field is required" />
              </FormControl>
            </ModalBody>
            <ModalFooter>
              <Button type="submit" colorScheme="blue" mr={3}>
                Save
              </Button>
              <Button onClick={onClose}>Cancel</Button>
            </ModalFooter>
          </ModalContent>
        </Formiz>
      </Modal>
    </>
  );
}

export default TodoList;
