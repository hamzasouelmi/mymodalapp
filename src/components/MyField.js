import React from 'react';
import { useField } from '@formiz/core';
import { Input } from '@chakra-ui/react';
import { FormGroup } from './FormGroup';

export const MyField = (props) => {
  const { errorMessage, id, isValid, setValue, value, otherProps } =
    useField(props);

  const { children, label, type, placeholder, required, ...restOtherProps } =
    otherProps;

  const formGroupProps = {
    label,
    errorMessage,
    isError: isValid,
    isRequired: !!required,
    ...restOtherProps,
  };

  return (
    <FormGroup {...formGroupProps}>
      <Input
        type="text"
        id={id}
        value={value ?? ''}
        onChange={(e) => setValue(e.target.value)}
        placeholder={placeholder}
      />

      {children}
    </FormGroup>
  );
};
