import {
  Button,
  HStack,
  Input,
  Text,
  useToast,
  VStack,
} from '@chakra-ui/react';
import React, { useState } from 'react';
import { nanoid } from 'nanoid';
import { Formiz, useForm } from '@formiz/core';
import { MyField } from './MyField';

function AddTodo({ addTodo }) {
  const myForm = useForm();

  function handleSubmit(e) {
    // e.preventDefault();
    /* if (!content) {
      toast({
        title: 'No content',
        status: 'error',
        duration: 2000,
        isClosable: true,
      });
      return;
    }*/

    addTodo({
      id: nanoid(),
      body: myForm.values.todo,
    });

    console.log(myForm.values);
  }
  const [isLoading, setIsLoading] = React.useState(false);

  const submitForm = (values) => {
    setIsLoading(true);
  };

  return (
    <Formiz onValidSubmit={handleSubmit} connect={myForm}>
      <form noValidate onSubmit={myForm.submit}>
        <VStack width="100%" alignItems={'strech'} mt="5">
          <MyField
            name="todo"
            label="enter what todo"
            required=" is required"
          />
          <Button
            colorScheme="green"
            px="5"
            type="submit"
            disabled={isLoading || (!myForm.isValid && myForm.isSubmitted)}
          >
            Add Todo
          </Button>
        </VStack>
      </form>
    </Formiz>
  );
}

export default AddTodo;
